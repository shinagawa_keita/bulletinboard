package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/isWorking" })
public class IsWorkingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("userId")));
		request.setAttribute("editUser", editUser);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		User userStatus = getEditUser(request);

		new UserService().deadOrAlive(userStatus);

		response.sendRedirect("management");

	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();

		editUser.setId(Integer.parseInt(request.getParameter("userId")));

		if(Integer.parseInt(request.getParameter("isWorking")) == 1) {
			editUser.setIsWorking(Integer.parseInt(request.getParameter("workUser")));
		} else {
			editUser.setIsWorking(Integer.parseInt(request.getParameter("stopUser")));
		}
		return editUser;
	}
}