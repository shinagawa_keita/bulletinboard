package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		int isWorking = new User().getIsWorking();

		LoginService loginService = new LoginService();

		User user = loginService.login(loginId, password, isWorking);

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			if (user != null) {
				session.setAttribute("loginUser", user);
				response.sendRedirect("./");

			} else {
				messages.add("ログインIDまたはパスワードが誤っています");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("loginId", loginId);
				request.getRequestDispatcher("login.jsp").forward(request, response);
			}

		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("loginId", loginId);
			request.getRequestDispatcher("login.jsp").forward(request, response);

		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		if (StringUtils.isEmpty(loginId) == true || StringUtils.isBlank(loginId) == true
				|| StringUtils.isEmpty(password) == true || StringUtils.isBlank(password) == true) {
			messages.add("ログインIDまたはパスワードが入力されていません");
		}

		if (messages.size() == 0) {
			return true;

		} else {
			return false;

		}
	}

}
