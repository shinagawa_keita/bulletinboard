package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Office;
import beans.Position;
import beans.User;
import service.OfficeService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<Office> offices = new OfficeService().getOfficeList();
		List<Position> positions = new PositionService().getPositionList();

		request.setAttribute("offices", offices);
		request.setAttribute("positions", positions);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{

		List<String> messages = new ArrayList<String>();

		List<Office> offices = new OfficeService().getOfficeList();
		List<Position> positions = new PositionService().getPositionList();

		//セッションを取得
		HttpSession session = request.getSession();

		String loginId = request.getParameter("loginId");

		if(new UserService().checkDuplicate(loginId) != null) {
			messages.add("このIDは既に使用されています");
		}

		if(isValid(request, messages) == true) {

			User user = new User();

			//JSPに入力された値をUserに格納
			user.setLoginId(request.getParameter("loginId"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setOfficeId(Integer.parseInt(request.getParameter("officeId")));
			user.setPositionId(Integer.parseInt(request.getParameter("positionId")));

			new UserService().register(user);

			response.sendRedirect("management");

		} else {

			//入力欄に不備があればエラーメッセージを表示してリダイレクト
			session.setAttribute("errorMessages", messages);
			request.setAttribute("loginId", request.getParameter("loginId"));
			request.setAttribute("name", request.getParameter("name"));
			request.setAttribute("positions", positions);
			request.setAttribute("offices", offices);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}

	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String confirmationPassword = request.getParameter("confirmationPassword");
		int positionId = Integer.parseInt(request.getParameter("positionId"));
		int officeId = Integer.parseInt(request.getParameter("officeId"));

		if (StringUtils.isBlank(loginId) == true) {
			messages.add("ログインIDを入力してください");
		}
		else if (loginId.length() < 6) {
			messages.add("ログインIDは6文字以上で入力してください");
		}
		else if (loginId.length() > 20) {
			messages.add("ログインIDは20文字以下で入力してください");
		}
		else if (!loginId.matches("^[0-9a-zA-Z]+")) {
			messages.add("ログインIDは半角英数字で入力してください");
		}

		if (StringUtils.isBlank(name) == true) {
			messages.add("ユーザー名を入力してください");
		}
		else if(name.length() > 10) {
			messages.add("ユーザー名は10文字以下で入力してください");
		}

		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		}
		else if (!password.matches("[-_@+*;:#$%&A-Za-z0-9]+")) {
			messages.add("パスワードは半角英数字(記号含む)で入力してください");
		}
		else if (!password.equals(confirmationPassword)) {
			messages.add("パスワードと確認用パスワードが一致しません");
		}
		else if(password.length() < 6 || password.length() > 20) {
			messages.add("パスワードは6文字以上で入力してください");
		}
		else if(password.length() > 20) {
			messages.add("パスワードは20文字以下で入力してください");
		}

		if (officeId == 1 && positionId == 3) {
			messages.add("勤務支店と部署・役職の組み合わせが不正です");
		}
		if (officeId == 1 && positionId == 4) {
			messages.add("勤務支店と部署・役職の組み合わせが不正です");
		}
		if (officeId != 1 && positionId == 1) {
			messages.add("勤務支店と部署・役職の組み合わせが不正です");
		}
		if (officeId != 1 && positionId == 2) {
			messages.add("勤務支店と部署・役職の組み合わせが不正です");
		}

		if (messages.size() == 0) {
			return true;

		} else {
			return false;
		}
	}

}
