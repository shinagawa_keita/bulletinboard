package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Office;
import beans.Position;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.OfficeService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class UserSettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		List<Office> offices = new OfficeService().getOfficeList();
		List<Position> positions = new PositionService().getPositionList();

		request.setAttribute("offices", offices);
		request.setAttribute("positions", positions);

		try {
			if (new UserService().getUser(Integer.parseInt(request.getParameter("userId"))) != null) {

				User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("userId")));
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("setting.jsp").forward(request, response);
			} else {
				messages.add("該当ユーザーが存在しません");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("management");
			}
		} catch (NumberFormatException e) {

			messages.add("不正なURLが入力されました");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		List<Office> offices = new OfficeService().getOfficeList();
		List<Position> positions = new PositionService().getPositionList();

		User editUser = getEditUser(request);
		User user = new UserService().getUser(Integer.parseInt(request.getParameter("userId")));

		String loginId = request.getParameter("loginId");
		String editUserLoginId = user.getLoginId();

		if (new UserService().checkDuplicate(loginId) != null && !loginId.equals(editUserLoginId)) {
			messages.add("このIDは既に使用されています");
		}

		if (isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);

			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("setting.jsp").forward(request, response);
				return;
			}

			response.sendRedirect("management");

		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.setAttribute("positions", positions);
			request.setAttribute("offices", offices);
			request.getRequestDispatcher("setting.jsp").forward(request, response);

		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("userId")));
		editUser.setName(request.getParameter("name"));
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setOfficeId(Integer.parseInt(request.getParameter("officeId")));
		editUser.setPositionId(Integer.parseInt(request.getParameter("positionId")));

		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String confirmationPassword = request.getParameter("confirmationPassword");
		int positionId = Integer.parseInt(request.getParameter("positionId"));
		int officeId = Integer.parseInt(request.getParameter("officeId"));

		if (StringUtils.isBlank(loginId) == true) {
			messages.add("ログインIDを入力してください");
		}
		else if (loginId.length() > 20) {
			messages.add("ログインIDは20文字以下で入力してください");
		}
		else if (loginId.length() < 6) {
			messages.add("ログインIDは6文字以上で入力してください");
		}
		else if(!loginId.matches("^[0-9a-zA-Z]+")) {
			messages.add("ログインIDは半角英数字で入力してください");
		}
		if (!password.matches("[-_@+*;:#$%&A-Za-z0-9]+") && StringUtils.isBlank(password) != true) {
			messages.add("パスワードは半角英数字(記号含む)で入力してください");
		}
		else if(!password.equals(confirmationPassword)) {
			messages.add("パスワードと確認用パスワードが一致しません");
		}
		else if(password.length() > 20 && StringUtils.isBlank(password) != true) {
			messages.add("パスワードは20文字以下で入力してください");
		}
		else if(password.length() < 6 && StringUtils.isBlank(password) != true) {
			messages.add("パスワードは6文字以上で入力してください");
		}
		if ( StringUtils.isBlank(name) == true) {
			messages.add("ユーザー名を入力してください");
		}
		else if(name.length() > 10) {
			messages.add("ユーザー名は10文字以下で入力してください");
		}
		if (officeId == 1 && positionId == 3) {
			messages.add("勤務支店と部署・役職の組み合わせが不正です");
		}
		if (officeId == 1 && positionId == 4) {
			messages.add("勤務支店と部署・役職の組み合わせが不正です");
		}
		if (officeId != 1 && positionId == 1) {
			messages.add("勤務支店と部署・役職の組み合わせが不正です");
		}
		if (officeId != 1 && positionId == 2) {
			messages.add("勤務支店と部署・役職の組み合わせが不正です");
		}

		if (messages.size() == 0) {
			return true;

		} else {
			return false;
		}
	}
}
