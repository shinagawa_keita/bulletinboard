package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException,ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		Comment comment = new Comment();

		String commentText = request.getParameter("commentText");

		if(isValid(request, messages) == true) {

			User user = (User) request.getSession().getAttribute("loginUser");

			comment.setText(commentText);
			comment.setUserId(user.getId());
			comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));

			new CommentService().register(comment);

			response.sendRedirect("./");

		} else {

			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

        String commentText = request.getParameter("commentText");

        if (StringUtils.isEmpty(commentText) == true || StringUtils.isBlank(commentText) == true) {
            messages.add("コメントが入力されていません");
        }
        if(commentText.length() > 500) {
        	messages.add("コメントは500文字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;

        } else {
            return false;

        }
	}

}
