package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("message.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        Message message = new Message();

        String text = request.getParameter("text");
        String title = request.getParameter("title");
        String category = request.getParameter("category");

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");
            message.setTitle(title);
            message.setCategory(category);
            message.setText(text);
            message.setUserId(user.getId());

            new MessageService().register(message);

            response.sendRedirect("./");

        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("title", title);
            request.setAttribute("category", category);
            request.setAttribute("text", text);
            request.getRequestDispatcher("message.jsp").forward(request, response);
        }
	}

	  private boolean isValid(HttpServletRequest request, List<String> messages) {

	        String title = request.getParameter("title");
	        String category = request.getParameter("category");
	        String text = request.getParameter("text");

	        if (StringUtils.isEmpty(title) == true  || StringUtils.isBlank(title) == true) {
	            messages.add("件名を入力してください");
	        }
	        if (StringUtils.isEmpty(category) == true  || StringUtils.isBlank(category) == true) {
	            messages.add("カテゴリーを入力してください");
	        }
	        if (StringUtils.isEmpty(text) == true || StringUtils.isBlank(text) == true) {
	            messages.add("本文を入力してください");
	        }
	        if(title.length() > 30) {
	        	messages.add("件名は30文字以下で入力してください");
	        }
	        if(category.length() > 10) {
	        	messages.add("カテゴリーは10文字以下で入力してください");
	        }
	        if(text.length() > 1000) {
	        	messages.add("本文は1000文字以下で入力してください");
	        }
	        if (messages.size() == 0) {
	            return true;

	        } else {
	            return false;

	        }
	    }

}
