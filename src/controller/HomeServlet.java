package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String category = request.getParameter("category");

		if(StringUtils.isEmpty(startDate) == true) {
			startDate = "2000-1-1";
		}
		if(StringUtils.isEmpty(endDate) == true) {
			endDate = "2150-12-31";
		}

		List<UserMessage> messages = new MessageService().getMessage(startDate, endDate, category);

        List<UserComment> comments = new CommentService().getComment();

        request.setAttribute("messages", messages);
        request.setAttribute("comments", comments);

		request.setAttribute("startDate", request.getParameter("startDate"));
		request.setAttribute("endDate", request.getParameter("endDate"));
		request.setAttribute("category", request.getParameter("category"));

		request.getRequestDispatcher("home.jsp").forward(request, response);
	}



}
