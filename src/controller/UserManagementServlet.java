package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserInformation;
import service.UserService;

@WebServlet(urlPatterns = { "/management" })

public class UserManagementServlet extends HttpServlet {
	private final static long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<UserInformation> users = new UserService().getUserInformationList();

		request.setAttribute("users", users);

		request.getRequestDispatcher("management.jsp").forward(request, response);
	}
}
