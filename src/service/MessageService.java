package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	public void register(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {

			close(connection);
		}
	}

	public void remove(Message message) {

		Connection connection = null;

		try {
			connection  = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.delete(connection, message);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	public List<UserMessage> getMessage(String startDate, String endDate, String category) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserMessageDao userMessageDao = new UserMessageDao();
	        List<UserMessage> ret = userMessageDao.getUserMessages(connection, startDate, endDate, category);

	        commit(connection);

	        return ret;

	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;

	    } catch (Error e) {
	        rollback(connection);
	        throw e;

	    } finally {
	        close(connection);

	    }
	}
}
