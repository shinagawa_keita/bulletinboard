package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Office;
import dao.OfficeDao;

public class OfficeService {

	public List<Office> getOfficeList() {

    	Connection connection = null;
	    try {
	        connection = getConnection();

	        OfficeDao officeDao = new OfficeDao();
	        List<Office> ret = officeDao.getOfficeList(connection);

	        commit(connection);

	        return ret;

	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;

	    } catch (Error e) {
	        rollback(connection);
	        throw e;

	    } finally {
	        close(connection);

	    }
    }
}
