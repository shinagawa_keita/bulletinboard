package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginCheckFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		List<String> messages = new ArrayList<String>();

		User user = (User) ((HttpServletRequest)request).getSession().getAttribute("loginUser");

		if(user == null && !((HttpServletRequest)request).getServletPath().equals("/login") && !((HttpServletRequest)request).getServletPath().equals("/css/style.css")) {
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse)response).sendRedirect("login");
			return;
		} else {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void init(FilterConfig config) throws ServletException {}

	@Override
	public void destroy() {}
}
