package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Office;
import exception.SQLRuntimeException;

public class OfficeDao {

	public List<Office> getOfficeList(Connection connection) {

		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM offices ORDER BY id";

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<Office> ret = toOfficeList(rs);

			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	private List<Office> toOfficeList(ResultSet rs) throws SQLException {

		List<Office> ret = new ArrayList<Office>();

		try {
			while (rs.next()) {

				int id = rs.getInt("id");
				String name = rs.getString("name");

				Office office = new Office();

				office.setId(id);
				office.setName(name);

				ret.add(office);
			}
			return ret;

		} finally {
			close(rs);
		}
	}

}
