package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import beans.UserInformation;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", office_id");
			sql.append(", position_id");
			sql.append(", is_working");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?"); //login_id
			sql.append(", ?"); //password
			sql.append(", ?"); //name
			sql.append(", ?"); //office_id
			sql.append(", ?"); //position_id
			sql.append(", 0"); //is_working
			sql.append(", CURRENT_TIMESTAMP"); //created_date
			sql.append(", CURRENT_TIMESTAMP"); //updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getOfficeId());
			ps.setInt(5, user.getPositionId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");

			if (!StringUtils.isEmpty(user.getPassword())) {
				sql.append(", password = ?");
			}

			sql.append(", name = ?");
			sql.append(", office_id = ?");
			sql.append(", position_id = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());

			if (!StringUtils.isEmpty(user.getPassword())) {

				ps.setString(2, user.getPassword());
				ps.setString(3, user.getName());
				ps.setInt(4, user.getOfficeId());
				ps.setInt(5, user.getPositionId());
				ps.setInt(6, user.getId());

			} else {

				ps.setString(2, user.getName());
				ps.setInt(3, user.getOfficeId());
				ps.setInt(4, user.getPositionId());
				ps.setInt(5, user.getId());
			}

			int count = ps.executeUpdate();

			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void deadOrAlive(Connection connection, User user) {

		PreparedStatement ps = null;

		try {
			String sql = "UPDATE users SET is_working = ? WHERE id = ?";

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getIsWorking());
			ps.setInt(2, user.getId());

			int count = ps.executeUpdate();

			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/*public List<User> getUserList(Connection connection) {

		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM users";

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<User> ret = toUserList(rs);

			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}*/

	public List<UserInformation> getUserInformationList(Connection connection) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("positions.name as position_name, ");
			sql.append("offices.name as office_name, ");
			sql.append("users.is_working as is_working ");
			sql.append("FROM users ");
			sql.append("INNER JOIN positions ");
			sql.append("ON positions.id = users.position_id ");
			sql.append("INNER JOIN offices ");
			sql.append("ON offices.id = users.office_id ");
			sql.append("ORDER BY created_date");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
            List<UserInformation> ret = toUserInformationList(rs);

            return ret;

		} catch(SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	private List<UserInformation> toUserInformationList(ResultSet rs) throws SQLException {

		List<UserInformation> ret = new ArrayList<UserInformation>();

		try {
			while (rs.next()) {

				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String officeName = rs.getString("office_name");
				String positionName = rs.getString("position_name");
				int isWorking = rs.getInt("is_working");

				UserInformation user = new UserInformation();

				user.setId(id);
				user.setLoginId(loginId);
				user.setName(name);
				user.setOfficeName(officeName);
				user.setPositionName(positionName);
				user.setIsWorking(isWorking);

				ret.add(user);
			}
			return ret;

		} finally {
			close(rs);
		}
	}

	public User getUser(Connection connection, String loginId, String password, int isWorking) {

		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? AND is_working = 0";
			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			List<User> userList = toUserList(rs);

			if (userList.isEmpty() == true) {
				return null;

			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");

			} else {
				return userList.get(0);

			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if (userList.isEmpty() == true) {
				return null;

			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");

			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();

		try {
			while (rs.next()) {

				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int officeId = rs.getInt("office_id");
				int positionId = rs.getInt("position_id");
				int isWorking = rs.getInt("is_working");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				User user = new User();

				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setOfficeId(officeId);
				user.setPositionId(positionId);
				user.setCreatedDate(createdDate);
				user.setIsWorking(isWorking);
				user.setUpdatedDate(updatedDate);

				ret.add(user);
			}
			return ret;

		} finally {
			close(rs);
		}
	}

	public User checkDuplication(Connection connection, String loginId) {

		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM users WHERE login_id = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if (userList.isEmpty() == true) {
				return null;

			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");

			} else {
				return userList.get(0);
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
