package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, String startDate, String endDate, String category) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.title as title, ");
            sql.append("messages.category as category, ");
            sql.append("messages.text as text, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE  messages.created_date BETWEEN ? AND ?");
            if(StringUtils.isEmpty(category) != true) {
                sql.append(" AND category like ?");
            }
            sql.append(" ORDER BY created_date DESC");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, startDate + " 00:00:00");
            ps.setString(2, endDate + " 23:59:00");

            if(StringUtils.isEmpty(category) != true) {

            	ps.setString(3, "%" + category + "%");
            }

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);

            return ret;


        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                String category = rs.getString("category");
                String text = rs.getString("text");
                int userId = rs.getInt("user_id");
                String name = rs.getString("name");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserMessage message = new UserMessage();
                message.setId(id);
                message.setTitle(title);
                message.setCategory(category);
                message.setText(text);
                message.setUserId(userId);
                message.setName(name);
                message.setCreatedDate(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}