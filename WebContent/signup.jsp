<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset= UTF-8">
		<title>新規登録</title>
	</head>
		<body>
			<div class = "main-contents">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
						<c:forEach items="${ errorMessages }" var="message">
							<li><c:out value="${ message }" /></li>
						</c:forEach>
				</div><br/>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

				<div class="header">
				<ul>
					<li><a href="logout">ログアウト</a></li>
					<li><a class="active" href="management">ユーザー管理</a></li>
					<li><a href="message">新規メッセージ投稿</a></li>
					<li><a href="./">ホーム</a></li>
					<li><font color="white"><c:out value="${loginUser.name}"/></font></li>
				</ul>
			</div><br/>

			<div class = "signUpForm">
				<form action = "signup" method = "post"><br/>
					<label for = "loginId">ログインID</label><br/><input name ="loginId" id = "loginId" value = "${ loginId }"><br/><br/>
					<label for = "name">ユーザー名</label><br/><input name ="name" id = "name" value = "${ name }"><br/><br/>
					<label for = "password">パスワード</label><br/><input name = "password" type = "password" id = "password"><br/><br/>
					<label for = "confirmationPassword">確認用パスワード</label><br/><input name = "confirmationPassword" type = "password" id = "confirmationPassword"><br/><br/>

					<label for = "office">勤務支店を選択してください</label><br/><br/>
					<div class = "selectBox">
						<select name = "officeId">
							<c:forEach items = "${ offices }" var = "office">
								<option value = "${ office.id }">${ office.name }</option>
							</c:forEach>
						</select><br/><br/>

						<label for = "office">部署・役職を選択してください</label><br/><br/>
						<select name = "positionId">
							<c:forEach items = "${ positions }" var = "position">
								<option value = "${ position.id }">${ position.name }</option>
							</c:forEach>
						</select><br/><br/>
					</div>

					<input type ="submit" value = "登録" /><br/><br/>
					<a href = "management">戻る</a>
				</form>
			</div>
			</div>
		</body>
</html>