<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset= UTF-8">
		<title>ユーザー情報編集</title>
	</head>
	<body>
		<div class = "main-contens">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
						<c:forEach items="${ errorMessages }" var="message">
							<li><c:out value="${ message }" /></li>
						</c:forEach>
				</div><br/>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="header">
				<ul>
					<li><a href="logout">ログアウト</a></li>
					<li><a class="active" href="management">ユーザー管理</a></li>
					<li><a href="message">新規メッセージ投稿</a></li>
					<li><a href="./">ホーム</a></li>
					<li><font color="white"><c:out value="${loginUser.name}"/></font></li>
				</ul>
			</div>

			<div class = "userSettingForm">
			<form action = "setting" method = "post"><br/>
				<input name = "userId" value = "${ editUser.id }" id = "id" type = "hidden">

				<label for = "loginId">ログインID</label><br/>
				<input name="loginId" value="${editUser.loginId}" id="loginId"/><br /><br/>
				<label for = "name">ユーザー名</label><br/>
				<input name= "name" value="${editUser.name}" id= "name"/><br /><br/>
				<label for = "password">パスワード</label><br/>
				<input name= "password" type = "password" id="password"/><br /><br/>
				<label for = "confirmationPassword">確認用パスワード</label><br/>
				<input name = "confirmationPassword" type = "password" id = "confirmationPassword"><br/><br/>

				<c:if test = "${ loginUser.id != editUser.id }">
					<label for = "officeId">勤務支店を選択してください</label><br/><br/>
					<select name = "officeId">
						<c:forEach items = "${ offices }" var = "office">
						<c:choose>
						<c:when test="${editUser.officeId == office.id }">
							<option value = "${ office.id }" selected>${ office.name }</option>
						</c:when>
						<c:otherwise>
							<option value = "${ office.id }">${ office.name }</option>
						</c:otherwise>
						</c:choose>
						</c:forEach>
					</select><br/><br/>

					<label for = "positionId">部署・役職を選択してください</label><br/><br/>
					<select name = "positionId">
					<!-- 編集ユーザーのポジションIDとForEachで回すIDが一致したら -->
						<c:forEach items = "${ positions }" var = "position">
						<c:choose>
						<c:when test="${editUser.positionId == position.id }">
							<option value = "${ position.id }" selected>${ position.name }</option>
						</c:when>
						<c:otherwise>
							<option value = "${ position.id }">${ position.name }</option>
						</c:otherwise>
						</c:choose>
						</c:forEach>

					</select><br/><br/>
				</c:if>

				<c:if test = "${ loginUser.id == editUser.id }">
					<input name = "officeId" value = "${ editUser.officeId }" type = "hidden">
					<input name = "positionId" value = "${ editUser.positionId }" type = "hidden">
				</c:if>


				<input type="submit" value="登録" /><br /><br/>
				<a href="management">戻る</a>
			</form>
		</div>
		</div>
	</body>
</html>