<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset= UTF-8">
		<title>ユーザー管理</title>
		<script type="text/javascript">

			function disp(){

				// 「OK」時の処理開始 ＋ 確認ダイアログの表示
				if(window.confirm('本当によろしいですか？')) {
					return true;

				} else {
					return false;

				}
			}

		</script>

	</head>
	<body>
		<div class="main-contens">
			<c:if test = "${ not empty errorMessages }">
				<div class = "errorMessages">
					<ul>
						<c:forEach items = "${ errorMessages }" var = "message">
							<li><c:out value = "${ message }" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var = "errorMessages" scope = "session" />
			</c:if>

			<div class="header">
				<ul>
					<li><a href="logout">ログアウト</a></li>
					<li><a class="active" href="management">ユーザー管理</a></li>
					<li><a href="message">新規メッセージ投稿</a></li>
					<li><a href="./">ホーム</a></li>
					<li><font color="white"><c:out value="${loginUser.name}"/></font></li>
				</ul>
			</div><br/><br/>

			<div class = "usersInformation">
				<table class = "userTable" border = "1">
					<tr>
						<th bgcolor="white" width="160"><font color="black">ログインID</font></th>
						<th bgcolor="white" width="160"><font color="black">ユーザー名</font></th>
						<th bgcolor="white" width="130"><font color="black">勤務支店</font></th>
						<th bgcolor="white" width="150"><font color="black">部署・役職</font></th>
						<th bgcolor="white" width="250"><font color="black">アカウント状態</font></th>
						<th bgcolor="white" width="230"><font color="black">ユーザー編集</font></th>
					</tr>

					<c:forEach items = "${ users }" var = "user">
						<tr>
							<td bgcolor="white" align="center" nowrap><span class = "user-id"><c:out value = "${ user.loginId }"/></span></td>
							<td bgcolor="white" align="center" nowrap><span class = "user-name"><c:out value = "${ user.name }" /></span></td>
							<td bgcolor="white" align="center" nowrap><span class = "user-office"><c:out value = "${ user.officeName }"/></span></td>
							<td bgcolor="white" align="center" nowrap><span class = "user-position"><c:out value = "${ user.positionName }"/></span></td>


							<td bgcolor="white" align="center" nowrap>
								<span class = "user-deadOrAlive">
									<c:if test = "${ loginUser.id != user.id }">
										<form action = "isWorking" method = "post" onSubmit = "return disp()" style="display:inline;">
											<input type = "hidden" name = "userId" value = "${ user.id }">
			 									<c:if test = "${ user.isWorking == 0 }">
			 										<input type = "hidden" name = "isWorking" value = "0">
													<input type = "hidden" name = "stopUser" value = "1">
													<input type = "submit" value = "停止">
												</c:if>

												<c:if test = "${ user.isWorking == 1 }">
													<input type = "hidden" name = "isWorking" value = "1">
													<input type = "hidden" name = "workUser" value = "0">
													<input type = "submit" value = "復活">
												</c:if>
										</form>
									</c:if>
									<c:if test = "${ loginUser.id == user.id }">
										<label for ="name"> ─ </label>
									</c:if>
								</span>
							</td>
							<td align="center" bgcolor="white" nowrap>
								<span class = "user-setting">
									<form action = "setting" method = "get" style="display:inline;">
										<input type = "hidden" name = "userId" value = "${ user.id }">
										<input type = "submit" value = "編集" >
										<a href = "setting?${user.id}" ></a>
									</form>
								</span>
							</td>
						</tr>
					</c:forEach>
				</table>
			</div><br/>

 			<div class ="signUpButton">
			<input type="button" onclick="location.href='signup'"value="ユーザー新規登録" style = "background-color:pink;"><br/><br/>
			<a href = "./">戻る</a>
			</div>
		</div>
	</body>
</html>