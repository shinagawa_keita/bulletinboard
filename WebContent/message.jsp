<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿</title>
	</head>
	<body>
		<div class = "main-contens">
			<c:if test = "${ not empty errorMessages }">
					<div class = "errorMessages">
							<c:forEach items = "${ errorMessages }" var = "message">
								<li><c:out value="${ message }" />
							</c:forEach>
					</div><br/>
				<c:remove var = "errorMessages" scope = "session" />
			</c:if>

			<div class="header">
				<ul>
					<li><a href="logout">ログアウト</a></li>
					<li><a href="management">ユーザー管理</a></li>
					<li><a class="active" href="message">新規メッセージ投稿</a></li>
					<li><a href="./">ホーム</a></li>
					<li><font color="white"><c:out value="${loginUser.name}"/></font></li>
				</ul>
			</div><br/>

			<div class="form-area">
				<div class  = "newMessageForm">
        		<form action="message" method="post">
            		<label for = "title">件名(30文字まで)</label><br/><input name = "title" id = "title" value = "${ title }"/><br/><br/>
            		<label for = "category">カテゴリー(10文字まで)</label><br/><input name = "category" id = "category" value = "${ category }" /><br/><br/>
            		メッセージを入力(1000文字まで)<br/><textarea name="text" cols="80" rows="10" class="message-textBox">${ text }</textarea><br/><br/>
            		<input type="submit" value="投稿"><br/><br/>
            		<a href = "./">戻る</a>
        		</form>
        		</div>
    		</div>
    	</div>
	</body>
</html>