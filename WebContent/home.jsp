<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<link href="./css/style.css" rel="stylesheet" type="text/css">

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ホーム</title>
	<script type="text/javascript">

			function disp1(){

				// 「OK」時の処理開始 ＋ 確認ダイアログの表示
				if(window.confirm('投稿を削除しますか？')) {
					return true;

				} else {
					return false;

				}
			}

			function disp2(){

				// 「OK」時の処理開始 ＋ 確認ダイアログの表示
				if(window.confirm('コメントを削除しますか？')) {
					return true;

				} else {
					return false;

				}
			}

		</script>
</head>
	<body>
		<div class="main-contens">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
						<c:forEach items="${ errorMessages }" var="message">
							<li><c:out value="${ message }" /></li>
						</c:forEach>
				</div><br/>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="header">
				<ul>
					<li><a href="logout">ログアウト</a></li>
					<li><a href="management">ユーザー管理</a></li>
					<li><a href="message">新規メッセージ投稿</a></li>
					<li><a class = "active" href="./">ホーム</a></li>
					<li><font color="white"><c:out value="${loginUser.name}"/></font></li>
				</ul>
			</div><br/>

			<div class = "search">
				<form action = "./" method = "get">
				<h1><p><span class="under">投稿検索</span></p></h1>
					<label for = "dateSearch"><h3>日付を指定</h3></label>
						<input type = "date" name = "startDate" value = "${ startDate }"> ～
						<input type = "date" name = "endDate" value = "${ endDate }"><br/><br/>
					<label for = "categorySearch"><h3>カテゴリーを指定(部分一致でも可)</h3></label>
						<input name = "category" id = "category" value = "${ category }"><br/><br/>
						<input type = "submit" value = "検索">
				</form>
			</div><br/>

			<div class="messages">
				<c:forEach items="${ messages }" var = "message" >
					<div class = "eachMessage">
						<div class="message-information">
							<h2><div class="title"><c:out value="${message.title}" /></div></h2>
							<h3><div class="category"><c:out value="${message.category}" /></div></h3><br/>
							<span class="messageText">
							<c:forEach var = "m" items = "${fn:split(message.text, '
	')}">
							<c:out value = "${ m }" /><br/><br/>
							</c:forEach>
							</span><br/>
						</div>

						<div class = "comments">
							<c:forEach items="${comments}" var="comment">
							<div  class = "eachComment">
								<div class="comment-view">
									<c:if test = "${ comment.messageId  == message.id }">
										<span class="commentText">
											<c:forEach var = "c" items = "${ fn:split(comment.text, '
')}">
												<c:out value="${c}"/><br/>
											</c:forEach>
										</span><br/>
									<span class="name">　Commented by　<c:out value="${comment.name}" /></span>

									<span class = "date" >(<fmt:formatDate  value="${comment.createdDate}" pattern="MM/dd HH:mm" />)</span>


										<span class = "commentDelete-button">
											<form action = "commentDelete" method = "post" onSubmit = "return disp2()" style="display: inline">
												<c:if test = "${ loginUser.id == comment.userId }">
													<input type = "hidden" name = "commentDeleteId" value = "${ comment.id }">
													<input type = "submit" value  = "コメント削除">
												</c:if>
											</form>
										</span><br/><br/>
									</c:if>
								</div>
							</div>
							</c:forEach>

							<div class="comment-textBox">
								<form action="comment" method="post">
									<textarea name="commentText" cols="60" rows="6"class="commentText"></textarea>
									<input type = "hidden" name = "messageId" value = "${ message.id }"><br/>
									<input type="submit" value="コメントする">
								</form>
							</div>

							<span class="name">投稿者：<c:out value="${message.name}" /></span>
							<span class="date">
								(<fmt:formatDate  value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm" />　投稿)
							</span>

							<span class = "messageDelete-button">
								<form action = "messageDelete" method = "post" onSubmit = "return disp1()" style="display: inline">
									<c:if test = "${ loginUser.id == message.userId }">
										<input type = "hidden" name = "deleteMessageId" value = "${ message.id }">
										　<input type = "submit" value = "投稿削除"><br/>
									</c:if>
								</form>
							</span><br/>
						</div>
					</div><br/>
				</c:forEach>
			</div>
		</div>
	</body>
</html>