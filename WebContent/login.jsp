<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログイン</title>
	</head>
	<body>
		<div class = "main-contens">

			<c:if test = "${ not empty errorMessages }">
				<div class = "errorMessages">
						<c:forEach items = "${ errorMessages }" var = "message">
							<li><c:out value="${ message }" />
						</c:forEach>
				</div>
				<c:remove var = "errorMessages" scope = "session" />
			</c:if><br/>
			
			<div class = "welcome">
				<label for = "welcome"><h1>Welcome</h1></label>
				<form action = "login" method = "post"><br/>
					<div class = "loginForm">
						<label for = "loginId">ログインID</label><br/><input name = "loginId" id = "loginId" value = "${ loginId }" /><br/><br/>
						<label for = "password">パスワード</label><br/><input name = "password" type = "password" id = "password"><br/><br/>
						<input type = "submit" value = "ログイン" /><br/>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>